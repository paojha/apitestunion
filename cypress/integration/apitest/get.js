/// <reference types = "Cypress" />

describe('get api user tests', ()=>{

    it('get users',()=>{

        cy.request({
            
            method : 'GET',
            url : 'https://docs.union54.technology/reference/getintegratordeposit-2',
            
            header : {
                'Authorization': "Bearer  34523236346346356356",
                'Accept': "application/json"
            } 

    }).then((res)=>{
        expect(res.status).to.eq(200)
    })
})

})