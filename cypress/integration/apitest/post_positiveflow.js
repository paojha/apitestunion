/// <reference types = "Cypress" />

describe('post user request', ()=>{
    let randomText = ""
    let testEmail = ""

    it('create user test',()=>{

        //Email is field which needs to be changed for every run. Code is written below which randomly generate new email ID for every run and no manual intervention is required.
        var pattern = "AFDGSDHDFJHGFJGJGDJKGDGJKDjfdjryjryjhojofhjorhjrokglk"
        for (var i = 0; i < 100; i++)
        randomText+=pattern.charAt(Math.floor(Math.random() * pattern.length));
        testEmail =  randomText + '@gmail.com'

        cy.request({
            
            method : 'POST',
            url : 'https://sandbox.union54.technology/v1/integrator/register',
            header : {
                'Authorization': "Bearer  34523236346346356356"
               // 'Accept': "application/json"
            },
            body: {
                "firstName": "Joe",
                "lastName": "Bloggs",
                "country": "ALB",
                "businessName": "Acme Systems Limited",
                "registrationNumber": "12345678",
                "businessAddress": "1 Street, City, Country",
                "domain": "domain.com",
                "webhookUrl": "https://integrator.com/union54/webhook",
                "password": "P@ssw0rd1234",
                "email": testEmail,
                "contactNumber": "12345678",
                "floatCurrencies": ["USD"]
            }

    }).then((res)=>{
        cy.log(JSON.stringify(res))
        expect(res.status).to.eq(201)
        expect(res.body.message).to.eq('Ok')
    })
})

})